<?php

function init_theme_functions() {

        register_post_type( 'biografia',
        array(
            'labels' => array(
                'name'              => _x( 'obra', 'taxonomy general name' ),
                'singular_name'     => _x( 'Obra', 'taxonomy singular name' ),
                'search_items'      => __( 'Buscar obra' ),
                'all_items'         => __( 'Todas las obras' ),
                'parent_item'       => __( 'Obra superior' ),
                'parent_item_colon' => __( 'Obra superior:' ),
                'edit_item'         => __( 'Editar obra' ),
                'update_item'       => __( 'Actualizar obra' ),
                'add_new_item'      => __( 'Añadir nueva obra' ),
                'new_item_name'     => __( 'Nombre de la obra' ),
                'menu_name'         => __( 'Obra' ),
            ),
            'taxonomies' => array('obra'),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'obra' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => true,
            'menu_position'      => null,
            'supports'           => array( 'title', 'editor', 'thumbnail', 'page-attributes', 'post-formats')
        )
    );

    register_post_type( 'mirada',
        array(
            'labels' => array(
                'name'              => _x( 'Miradas', 'taxonomy general name' ),
                'singular_name'     => _x( 'mirada', 'taxonomy singular name' ),
                'search_items'      => __( 'Buscar mirada' ),
                'all_items'         => __( 'Todas las miradas' ),
                'parent_item'       => __( 'Mirada superior' ),
                'parent_item_colon' => __( 'Mirada superior:' ),
                'edit_item'         => __( 'Editar mirada' ),
                'update_item'       => __( 'Actualizar mirada' ),
                'add_new_item'      => __( 'Añadir nueva mirada' ),
                'new_item_name'     => __( 'Nombre de la nueva mirada' ),
                'menu_name'         => __( 'Miradas' ),
            ),
            'taxonomies' => array('miradas'),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'miradas' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => true,
            'menu_position'      => null,
            'supports'           => array( 'title', 'editor', 'thumbnail', 'page-attributes', 'post-formats')
        )
    );

    register_post_type( 'conferencia',
        array(
            'labels' => array(
                'name'              => _x( 'Conferencias', 'taxonomy general name' ),
                'singular_name'     => _x( 'conferencia', 'taxonomy singular name' ),
                'search_items'      => __( 'Buscar conferencia' ),
                'all_items'         => __( 'Todas las conferencias' ),
                'parent_item'       => __( 'Conferencia superior' ),
                'parent_item_colon' => __( 'Conferencia superior:' ),
                'edit_item'         => __( 'Editar conferencia' ),
                'update_item'       => __( 'Actualizar conferencia' ),
                'add_new_item'      => __( 'Añadir nueva conferencia' ),
                'new_item_name'     => __( 'Nombre de la nueva conferencia' ),
                'menu_name'         => __( 'Conferencias' ),
            ),
            'taxonomies' => array('conferencias'),
            'public'             => true,
            'publicly_queryable' => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => array( 'slug' => 'conferencias' ),
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => true,
            'menu_position'      => null,
            'supports'           => array( 'title', 'editor', 'thumbnail', 'page-attributes', 'post-formats')
        )
    );


    global $wp_rewrite;
    //Call flush_rules() as a method of the $wp_rewrite object
    $wp_rewrite->flush_rules( false );

}

add_action( 'init', 'init_theme_functions' );


function special_nav_class( $classes, $item ) {

    $classes[] = 'menu-item-' . sanitize_title( $item->title);

    return $classes;

}

add_filter( 'nav_menu_css_class', 'special_nav_class', 10, 2 );

?>