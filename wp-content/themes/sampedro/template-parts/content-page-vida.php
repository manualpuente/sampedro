<?php
/**
 * The template used for displaying page content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php $page_parents = get_post_ancestors( $post->ID ); ?>
        <?php $id = ($page_parents) ? $page_parents[count($page_parents)-2]: $post->ID; ?>
        <?php if(has_post_thumbnail( $id )){ ?>
            <?php
            $image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'optional-size' );
            $image = $image_array[0];
            $custom_header_sizes = apply_filters( 'twentysixteen_custom_header_sizes', '(max-width: 709px) 85vw, (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px' );
            ?>
            <div class="header-image row">
                <img src="<?php echo $image; ?>"  alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
            </div><!-- .header-image -->
        <?php } ?>


    <?php if(is_page_child(get_page_by_title('Vida')->ID) ){ ?>
        <div id="sisters">
            <ul class="nav nav-tabs" role="tablist">
                <?php

                $vida = get_page_by_title('Vida');
                //$first_childrens = get_page_children( $pueblo->ID, $all_wp_pages);
                wp_list_pages( array(
                    // Only pages that are children of the current page
                    'child_of' => $vida->ID,
                    // Only show one level of hierarchy
                    'depth' => 2,
                    'title_li' => '',
                    'sort_column' =>  'menu_order'
                ) );
                ?>
            </ul>
        </div>
    <?php } ?>

    <div class="article-content">
        <header class="entry-header">
            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            <?php if (! is_page(get_page_by_title('Vida')->ID)){ ?>
                <?php if(has_post_thumbnail()){ ?>
                    <div class="header-image row">
                        <?php twentysixteen_post_thumbnail() ?>
                    </div>
                <?php } ?>
            <?php } ?>

        </header><!-- .entry-header -->

        <div class="entry-content">
            <?php
            the_content();

            wp_link_pages( array(
                'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
                'after'       => '</div>',
                'link_before' => '<span>',
                'link_after'  => '</span>',
                'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
                'separator'   => '<span class="screen-reader-text">, </span>',
            ) );
            ?>
        </div><!-- .entry-content -->

        <?php
            edit_post_link(
                sprintf(
                    /* translators: %s: Name of current post */
                    __( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
                    get_the_title()
                ),
                '<footer class="entry-footer"><span class="edit-link">',
                '</span></footer><!-- .entry-footer -->'
            );
        ?>

    </div>


</article><!-- #post-## -->
