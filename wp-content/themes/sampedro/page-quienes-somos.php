<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<?php if(has_post_thumbnail( $id )){ ?>
    <?php
    $image_array = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), 'optional-size' );
    $image = $image_array[0];
    $custom_header_sizes = apply_filters( 'twentysixteen_custom_header_sizes', '(max-width: 709px) 85vw, (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px' );
    ?>
    <div class="header-image row">
        <img src="<?php echo $image; ?>"  alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
    </div><!-- .header-image -->
<?php } ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'page-subpages' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>
<script>
    jQuery(document).ready(function(){
        jQuery('.nav-tabs').scrollingTabs();
    })
</script>
<?php get_footer(); ?>
